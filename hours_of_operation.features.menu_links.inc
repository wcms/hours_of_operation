<?php

/**
 * @file
 * hours_of_operation.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function hours_of_operation_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_hours-of-operation-settings:admin/config/system/hours_of_operation_settings.
  $menu_links['menu-site-management_hours-of-operation-settings:admin/config/system/hours_of_operation_settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/hours_of_operation_settings',
    'router_path' => 'admin/config/system/hours_of_operation_settings',
    'link_title' => 'Hours of operation settings',
    'options' => array(
      'attributes' => array(
        'title' => 'Link to edit hours of operation disclaimer text.',
      ),
      'identifier' => 'menu-site-management_hours-of-operation-settings:admin/config/system/hours_of_operation_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Hours of operation settings');

  return $menu_links;
}
